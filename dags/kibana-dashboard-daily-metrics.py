
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.kubernetes.secret import Secret
from airflow.utils.dates import days_ago
from datetime import datetime, timedelta

# date for yesterday
yesterday = (datetime.now() + timedelta(days=-1)).strftime("%d/%m/%Y")

# Secrets to env vars in pods
rclone_conf = Secret(
    deploy_type   = "volume",
    deploy_target = "/app/config/",
    secret        = "rclone-config",
    key           = "rclone.conf"
)

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "retries": 0,
}

# define DAG
dag = DAG(
    description = """Pipeline extracting metrics of kibana dashboard
        from nginx-ingress logs of Logdna and saving the results in COS""",
    dag_id="kibana-dashboard-daily-metrics",
    default_args=default_args,
    start_date=days_ago(1),
    schedule_interval=None,
    catchup=False,
    tags=["eli", "kibana", "dashboard metrics", "rclone"],
)

# Define tasks
with dag:
    start = DummyOperator(
        task_id = "start"
    )
    
    stop = DummyOperator(
        task_id = "stop"
    )

    # extract metrics
    get_metrics = KubernetesPodOperator(
            namespace               = "default",
            name                    = f"get_metrics",
            task_id                 = f"get_metrics",
            image                   = "elimayost/kibana-dashboard-daily-metrics:latest",
            image_pull_policy       = "Always",
            startup_timeout_seconds = 120,
            cmds                    = ["bash"],
            arguments               = ["/app/scripts/extract_data.sh", yesterday],
            secrets                 = [rclone_conf]
    )
    
start >> get_metrics >> stop

